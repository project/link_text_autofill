/**
 * @file
 * Contains autofill.js.
 */
(function ($, Drupal) {

  "use strict";

  Drupal.behaviors.LinkTextAutofillBehavior = {
    attach: function (context, drupalSettings) {

      // See https://www.drupal.org/node/3158256 for the syntax of once().
      $(once('link_text_autofill', '.link-text-autofill', context)).each(function () {
        // Attach a listener to all autocomplete elements inside this.
        const $all_autocompletes = $('input.form-autocomplete', this);
        $all_autocompletes.on('autocompleteclose', function (e) {
          const $this = $(this);
          const $text_input = $this.parents('.js-form-type-entity-autocomplete').siblings('.js-form-type-textfield').find('input');
          if ($text_input.length > 0 && $text_input.val() === '') {
            const label = $this.val();
            // Get rid of the "([entity id])" suffix.
            const matches = label.match(/^(.+)(\s\([0-9]+\))$/);
            if (matches !== null && matches.length > 2 && matches[1].length > 0) {
              let text = matches[1];
              // Respect maxlenght limitations, if they exist.
              if ($text_input.hasClass('maxlength') && $text_input.data('maxlength') > 0) {
                text = text.substring(0, $text_input.data('maxlength'));
              }
              // Store this string in the text input.
              $text_input.val(text);
              $text_input.trigger('change');
            }
          }
        });

      });

    }
  };

})(jQuery, Drupal, drupalSettings);
