# Link Text Autofill

## Overview

This simple module allows to configure link widgets to automatically copy the
referenced item's label into the field text, if empty.

## Configuration

- Install the module
- Navigate to the form display, and open the link widget configuration options.
- Choose "Auto-fill the link text with the referenced entity label, if empty."

## Theme compatibility

This module has only been tested in the Claro admin theme. If incompatibility
with other themes are found, patches are welcome in the issue queue.
